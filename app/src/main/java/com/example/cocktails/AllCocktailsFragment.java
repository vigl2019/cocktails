package com.example.cocktails;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.cocktails.model.Cocktail;
import com.example.cocktails.model.CocktailDetail;
import com.example.cocktails.utils.Coordinator;
import com.example.cocktails.utils.Helper;
import com.example.cocktails.utils.IHelperCocktailDetail;
import com.example.cocktails.utils.IHelperCocktails;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class AllCocktailsFragment extends Fragment {

    private Coordinator coordinator;
    private CocktailAdapter cocktailAdapter;
    private RecyclerView recyclerView;
    private Helper helper;
    private Context context;

    private IHelperCocktails iHelperCocktails = new IHelperCocktails() {
        @Override
        public void getCocktails(List<Cocktail> cocktails) {
            setCocktailAdapter(cocktails);
        }
    };

    private IHelperCocktailDetail iHelperCocktailDetail = new IHelperCocktailDetail() {
        @Override
        public void getCocktailDetail(CocktailDetail cocktailDetail) {
            coordinator.showCocktailDetails(cocktailDetail);
        }
    };

    //================================================================================//

    @Override
    public void onAttach(Context context){
        super.onAttach(context);

        this.context = context;

        if(context instanceof Coordinator) {
            coordinator = (Coordinator) context;
        }
        else {
            throw new IllegalArgumentException(context.getClass().getSimpleName() + " must implement interface Coordinator!");
        }
    }

    //================================================================================//

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//      return inflater.inflate(R.layout.fragment_all_cocktails, container, false);

        View root = inflater.inflate(R.layout.fragment_all_cocktails, container, false);
        recyclerView = root.findViewById(R.id.fac_cocktail_list);

        helper = new Helper(context);
        helper.getCocktailsFromServer(iHelperCocktails);

        return root;
    }

    //================================================================================//

    private void setCocktailAdapter(List<Cocktail> cocktails){
        cocktailAdapter = new CocktailAdapter(cocktails);
        recyclerView.setAdapter(cocktailAdapter);

        cocktailAdapter.setListener(new CocktailAdapter.OnItemClickListener() {
            @Override
            public void onClick(Cocktail cocktail) {
                helper.getCocktailFromServerByCocktailId(cocktail.getIdDrink(), iHelperCocktailDetail);
            }
        });
    }

    //================================================================================//

    @Override
    public void onStop() {
        super.onStop();

        helper.unSubscribe();
    }
}

