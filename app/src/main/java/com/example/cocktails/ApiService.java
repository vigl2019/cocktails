package com.example.cocktails;

import com.example.cocktails.model.CocktailDetail;
import com.example.cocktails.model.ResponseModel;

import io.reactivex.Flowable;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class ApiService {

    private static final String API = "https://www.thecocktaildb.com/api/json/v1/1/";

    private static PrivateApi privateApi;

    public interface PrivateApi {

        @GET("filter.php")
        Flowable<ResponseModel> getCocktails(@Query("a") String typeOfCocktail);

        @GET("lookup.php")
        Flowable<CocktailDetail> getCocktailById(@Query("i") String cocktailId);
    }

    static {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(API)
                .client(client)
                .build();

        privateApi = retrofit.create(PrivateApi.class);
    }

    public static Flowable<ResponseModel> getCocktails(String typeOfCocktail){
        return privateApi.getCocktails(typeOfCocktail);
    }

    public static Flowable<CocktailDetail> getCocktailById(String cocktailId){
        return privateApi.getCocktailById(cocktailId);
    }
}
