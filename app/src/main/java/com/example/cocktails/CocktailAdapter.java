package com.example.cocktails;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.cocktails.model.Cocktail;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CocktailAdapter extends RecyclerView.Adapter<CocktailAdapter.CocktailViewHolder> {

    private List<Cocktail> cocktails;
    private OnItemClickListener listener;

    interface OnItemClickListener {
        void onClick(Cocktail cocktail);
    }

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public CocktailAdapter(List<Cocktail> cocktails) {
        this.cocktails = cocktails;
    }

    //================================================================================//

    @NonNull
    @Override
    public CocktailAdapter.CocktailViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CardView cardView = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cocktail_cardview, parent, false);

        return new CocktailViewHolder(cardView);
    }

    @Override
    public void onBindViewHolder(@NonNull CocktailAdapter.CocktailViewHolder holder, int position) {
        CardView cardView = holder.cardView;
        Cocktail cocktail = cocktails.get(position);

        ImageView cocktailImage = (ImageView)cardView.findViewById(R.id.ccv_image);
        Picasso.get().load(cocktail.getStrDrinkThumb()).into(cocktailImage);

        TextView cocktailTitle = (TextView)cardView.findViewById(R.id.ccv_title);
        cocktailTitle.setText(cocktail.getStrDrink());
    }

    @Override
    public int getItemCount() {
        return cocktails.size();
    }

    //================================================================================//

    class CocktailViewHolder extends RecyclerView.ViewHolder {

        CardView cardView;

        public CocktailViewHolder(@NonNull CardView itemView) {
            super(itemView);

            cardView = itemView;

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(cocktails.get(getAdapterPosition()));
                }
            });
        }
    }
}
