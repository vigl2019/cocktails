package com.example.cocktails;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.cocktails.model.CocktailDetail;
import com.squareup.picasso.Picasso;

/**
 * A simple {@link Fragment} subclass.
 */
public class CocktailDetailsFragment extends Fragment {

    public static final String EXTRA_KEY_ID_DRINK = "idDrink";
    public static final String EXTRA_KEY_IMAGE = "image";
    public static final String EXTRA_KEY_Title = "title";
    public static final String EXTRA_KEY_INSTRUCTIONS = "instructions";

    private String idDrink;
    private String image;
    private String title;
    private String instructions;

    private ImageView cocktailImage;
    private TextView cocktailId;
    private TextView cocktailTitle;
    private TextView cocktailInstructions;

    private CocktailDetail cocktailDetail;

    public static CocktailDetailsFragment newInstance(String idDrink, String image, String title, String instructions) {

        Bundle args = new Bundle();

        args.putString(EXTRA_KEY_ID_DRINK, idDrink);
        args.putString(EXTRA_KEY_IMAGE, image);
        args.putString(EXTRA_KEY_Title, title);
        args.putString(EXTRA_KEY_INSTRUCTIONS, instructions);

        CocktailDetailsFragment fragment = new CocktailDetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle arg = getArguments();

        idDrink = arg.getString(EXTRA_KEY_ID_DRINK);
        image = arg.getString(EXTRA_KEY_IMAGE);
        title = arg.getString(EXTRA_KEY_Title);
        instructions = arg.getString(EXTRA_KEY_INSTRUCTIONS);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//      return inflater.inflate(R.layout.fragment_cocktail_details, container, false);

        View root = inflater.inflate(R.layout.fragment_cocktail_details, container, false);

        cocktailImage = root.findViewById(R.id.fcd_image);
        Picasso.get().load(image).into(cocktailImage);

        cocktailId = root.findViewById(R.id.fcd_id_drink);
        cocktailId.setText(idDrink);

        cocktailTitle = root.findViewById(R.id.fcd_title);
        cocktailTitle.setText(title);

        cocktailInstructions = root.findViewById(R.id.fcd_instructions);
        cocktailInstructions.setText(instructions);

        return root;
    }
}
