/*

Используя API сервиса https://www.thecocktaildb.com/api.php создать приложение с двумя экранами.
На первом отобразить список с названием коктейлей и их фотографией.
По клику на любой коктейль открывать второй экран.
На втором экране должно быть название коктейля, его фотка и подробное описание.

Для получения списка коктейлей использовать запрос
https://www.thecocktaildb.com/api/json/v1/1/filter.php?a=Alcoholic

Для получения деталей по выбранному коктейлю использовать запрос
https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i=17105, где 17105 - номер выбранного коктейля.

________________________________________


0. При создании приложения можно не использовать фрагменты :)

1. При создании приложения использовать фрагменты. Приложение должно работать только в портретном режиме.

2* При создании приложения использовать фрагменты. Приложение должно работать и в портретном режиме, и в альбомном.

3*** При создании приложения использовать фрагменты. В landscape режиме приложение должно использовать отдельную разметку,
на которой и список коктейлей, и детальная информация отображаются одновременно.

В этом случае полученный с сервера список с коктейлями нужно будет сохранить в bundle
https://ru.stackoverflow.com/questions/660859/Рабочий-пример-кода-с-onrestoreinstancestate

*/

package com.example.cocktails;

import android.content.res.Configuration;
import android.os.Bundle;
import android.widget.FrameLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.example.cocktails.model.CocktailDetail;
import com.example.cocktails.utils.Coordinator;

import java.util.List;

public class MainActivity extends AppCompatActivity implements Coordinator {

    FrameLayout container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        container = findViewById(R.id.root);

        if (savedInstanceState == null) { // If the first start of the Activity

            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                initPortrait();
            } else {
                initLandscape();
            }
        } else { // If there was already a launch of Activity and Fragments
            // restore state
        }
    }

    //================================================================================//

    private void initPortrait() {

        // new instance
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.root, new AllCocktailsFragment())
                .commit();

    }

    private void initLandscape() {
        // new instance
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.root, new AllCocktailsFragment())
                .commit();
/*
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.root_right, CocktailDetailsFragment.newInstance(""))
                .commit();
*/
    }

    //================================================================================//

    @Override
    public void showCocktailDetails(CocktailDetail cocktailDetail) {

        List<CocktailDetail.Drink> drinks = cocktailDetail.getDrinks();

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.root, CocktailDetailsFragment.newInstance(drinks.get(0).getIdDrink(), drinks.get(0).getStrDrinkThumb(),
                            drinks.get(0).getStrDrink(), drinks.get(0).getStrInstructions()))
                    .addToBackStack(null) // BackStack-ов может быть несколько, поэтому, чтобы не запутаться, можно BackStack установить в null
                    .commit();
        } else { // не отображаем новый экран, поэтому нет кнопки "назад"
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.root_right, CocktailDetailsFragment.newInstance(drinks.get(0).getIdDrink(), drinks.get(0).getStrDrinkThumb(),
                            drinks.get(0).getStrDrink(), drinks.get(0).getStrInstructions()))
                    .commit();
        }
    }
}