package com.example.cocktails.utils;

import com.example.cocktails.model.Cocktail;
import com.example.cocktails.model.ResponseModel;

import java.util.ArrayList;
import java.util.List;

public class Converter {

    public static List<Cocktail> convert(ResponseModel responseModel) {

        List<Cocktail> cocktails = new ArrayList<>();
        List<ResponseModel.Drink> drinks = responseModel.getDrinks();

        for (int i = 0; i < drinks.size(); i++) {

            cocktails.add(new Cocktail(drinks.get(i).getIdDrink(), drinks.get(i).getStrDrink(), drinks.get(i).getStrDrinkThumb()));
        }

        return cocktails;
    }
}

