package com.example.cocktails.utils;

import com.example.cocktails.model.CocktailDetail;

public interface Coordinator {
    void showCocktailDetails(CocktailDetail cocktailDetail);
}
