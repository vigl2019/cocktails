package com.example.cocktails.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.example.cocktails.ApiService;
import com.example.cocktails.R;
import com.example.cocktails.model.Cocktail;
import com.example.cocktails.model.CocktailDetail;
import com.example.cocktails.model.ResponseModel;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;

public class Helper {

    public static final String COCKTAIL_TYPE = "Alcoholic";

    private Context context;
    private CompositeDisposable disposables = new CompositeDisposable();

    public Helper(Context context) {
        this.context = context;
    }

    //================================================================================//

    public void getCocktailsFromServer(final IHelperCocktails iHelperCocktails) {

        disposables.add(ApiService.getCocktails(COCKTAIL_TYPE)

                .map(new Function<ResponseModel, List<Cocktail>>() {
                    @Override
                    public List<Cocktail> apply(ResponseModel responseModel) {

                        final List<Cocktail> cocktails = Converter.convert(responseModel);
                        return cocktails;
                    }
                })

                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())

                .filter(new Predicate<List<Cocktail>>() {
                    @Override
                    public boolean test(List<Cocktail> cocktails) throws Exception {

                        boolean isCocktailsHaveValues = ((cocktails != null) && (!cocktails.isEmpty()));

                        if (!isCocktailsHaveValues) {
                            Helper.createAlertDialog(context.getResources().getString(R.string.empty_cocktails_list), null, context);
                        }

                        return isCocktailsHaveValues;
                    }
                })

                .subscribe(new Consumer<List<Cocktail>>() {
                    @Override
                    public void accept(List<Cocktail> cocktails) throws Exception {
                        iHelperCocktails.getCocktails(cocktails);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        Toast.makeText(context, throwable.getMessage(), Toast.LENGTH_LONG).show();
//                        Toast.makeText(context, "Something went wrong", Toast.LENGTH_LONG).show();
                    }
                }));
    }

    //================================================================================//

    public void getCocktailFromServerByCocktailId(String cocktailId, final IHelperCocktailDetail iHelperCocktailDetail) {

        disposables.add(ApiService.getCocktailById(cocktailId)

                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())

                .filter(new Predicate<CocktailDetail>() {
                    @Override
                    public boolean test(CocktailDetail cocktailDetail) throws Exception {

                        boolean isCocktailDetailHaveValues = (cocktailDetail != null);

                        if (!isCocktailDetailHaveValues) {
                            Helper.createAlertDialog(context.getResources().getString(R.string.null_cocktail_detail), null, context);
                        }

                        return isCocktailDetailHaveValues;
                    }
                })

                .subscribe(new Consumer<CocktailDetail>() {
                    @Override
                    public void accept(CocktailDetail cocktailDetail) throws Exception {
                        iHelperCocktailDetail.getCocktailDetail(cocktailDetail);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        Toast.makeText(context, throwable.getMessage(), Toast.LENGTH_LONG).show();
//                        Toast.makeText(context, "Something went wrong", Toast.LENGTH_LONG).show();
                    }
                }));
    }

    //================================================================================//

    public void unSubscribe() {
        disposables.clear();
    }

    //================================================================================//

    public static void createAlertDialog(String errorMassage, final View view, Context context) {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        dialogBuilder.setTitle("Error!")
                .setMessage(errorMassage)
                .setCancelable(false)
                .setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();

                        if (view != null) {
                            view.requestFocus();
                        }
                    }
                });

        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }
}
