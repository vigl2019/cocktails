package com.example.cocktails.utils;

import com.example.cocktails.model.CocktailDetail;

public interface IHelperCocktailDetail {
    void getCocktailDetail(CocktailDetail cocktailDetail);
}
