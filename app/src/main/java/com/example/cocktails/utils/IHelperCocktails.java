package com.example.cocktails.utils;

import com.example.cocktails.model.Cocktail;

import java.util.List;

public interface IHelperCocktails {
    void getCocktails(List<Cocktail> cocktails);
}
